const express = require("express");

const app = express();


const errorMiddleware = require("./middlewares/errors");
const cookieParser = require("cookie-parser");

app.use(express.json({ extend: false }));
app.use(cookieParser());



//Import all routes
const products = require("./routes/product");

app.use(products);

app.use(errorMiddleware)

module.exports = app;