const express = require("express");
const router = express.Router();

const { 
    getProducts, 
    newProduct, 
    getSingleProduct, 
    updateProduct,
    deleteProduct,
    createProductReview,
    getProductReviews,
    deleteProductReviews
} =  require("../controllers/productController");

const { isAuthenticatedUser, authorizeRoles } = require("../middlewares/auth");

router.route("/products").get(isAuthenticatedUser, authorizeRoles("admin"), getProducts);
router.route("/:id").get(getSingleProduct);

router.route("/admin/product/new").post(isAuthenticatedUser, authorizeRoles("admin"), newProduct);

router.route("/admin/:id").put(isAuthenticatedUser, authorizeRoles("admin"), updateProduct)
                                    .delete(isAuthenticatedUser, authorizeRoles("admin"), deleteProduct);

router.route("/review").put(isAuthenticatedUser, createProductReview)
router.route("/reviews/:id").get(isAuthenticatedUser, getProductReviews)
router.route("/reviews").delete(isAuthenticatedUser, deleteProductReviews)




module.exports = router;