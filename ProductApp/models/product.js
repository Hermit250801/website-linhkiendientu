const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Vui lòng nhập tên sản phẩm'],
        trim: true,
    },
    price: {
        type: Number,
        required: [true, 'Vui lòng nhập giá sản phẩm'],
        default: 0.0
    },
    oldPrice: {
        type: Number,
        required: [true, 'Vui lòng nhập giá cũ của sản phẩm'],
        default: 0.0
    },
    description: {
        type: String,
        required: [true, 'Vui lòng nhập mô tả sản phẩm'],
    },
    ratings: {
        type: Number,
        default: 0
    },
    images: [
        {
            public_id: {
                type: String,
                required: true
            },
            url: {
                type: String,
                required: true
            },
        }
    ],
    category: {
        type: String,
        required: [true, 'Vui lòng chọn loại sản phẩm cho sản phẩm này'],
        enum: {
            values: [
                'Mainboard',
                'CPU',
                'VGA',
                'RAM',
                'Ổ cứng',
                'Nguồn máy tính',
                "Vỏ case",
                'Tản nhiệt',
                'Màn hình',
                'Ổ đĩa quang',
                'Tai nghe',
                'Chuột',
                'Bàn phím',
                'Màn hình'
            ],
            message: 'Vui lòng chọn đúng loại sản phẩm'
        }
    },
    seller: {
        type: String,
        required: [true, 'Vui lòng nhập người mua']
    },
    stock: {
        type: Number,
        required: [true, 'Vui lòng nhập số sản phẩm tồn kho'],
        default: 0
    },
    numOfReviews: {
        type: Number,
        default: 0
    },
    reviews: [
        {
            user: {
                type: mongoose.Schema.ObjectId,
                ref: 'User',
                required: true
            },
            name: {
                type: String,
                required: true
            },
            rating: {
                type: Number,
                required: true
            },
            comment: {
                type: String,
                required: true
            }
        }
    ],
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    warranty: {
        type: String,
        required: [true, 'Vui lòng nhập thời gian bảo hành'],
        default: "12 tháng"
    }
})

module.exports = mongoose.model("Product", productSchema)