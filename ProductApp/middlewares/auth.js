const ErrorHandler = require("../utils/errorHandler");
const cathAsyncErrors = require("./catchAsyncErrors");
const User = require("../models/user")
const jwt = require("jsonwebtoken")

//Check if user is authenicated or not
exports.isAuthenticatedUser = cathAsyncErrors(async (req, res, next) => {
    const { token } = req.cookies;

    if(!token) {
        return next(new ErrorHandler("Đăng nhập trước để truy cập.", 401))
    }


    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = await User.findById(decoded.id);
    next();
})

//Handling users roles
exports.authorizeRoles = (...roles) => {
    return (req, res, next) => {
        if(!roles.includes(req.user.role)) {
            return next(new ErrorHandler(`Role (${req.user.role}) không được phép truy cập vào tài nguyên này`, 403));
        }
        next()
    }
}
