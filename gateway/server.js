const express = require("express");
const proxy = require("express-http-proxy")

const PORT = 4000;

const app = express();

app.use(express.json({ extends: false }))
app.use("/api/v1/user", proxy("http://localhost:4001/"))
app.use("/api/v1/product", proxy("http://localhost:4002/"))
app.use("/api/v1/order", proxy("http://localhost:4003/"))

app.listen(PORT, () => {
    console.log(`App Running on PORT: ${PORT}`)
})