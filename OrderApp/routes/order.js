const express = require("express");
const router = express.Router();
const breaker = require('express-circuit-breaker');

var CB = breaker({
    catchError: e => 'trip',
    handleBlockedRequest: (req, res) => {
        res.sendStatus(500).json({
            message: "Circuit Breaker Open"
        })
    }
})


const { 
    newOrder,
    getSingleOrder,
    myOrders,
    allOrders,
    updateProcessOrder
} = require("../controllers/orderController");

const { isAuthenticatedUser, authorizeRoles } = require("../middlewares/auth");

router.route("/new").post(CB,isAuthenticatedUser, newOrder);
router.route("/:id").get(CB, isAuthenticatedUser, getSingleOrder);
router.route("/orders/me").get(CB, isAuthenticatedUser, myOrders);
router.route("/admin/orders").get(CB, isAuthenticatedUser, authorizeRoles("admin"),allOrders);
router.route("/admin/order/:id").put(CB, isAuthenticatedUser, authorizeRoles("admin"),updateProcessOrder);

module.exports = router;