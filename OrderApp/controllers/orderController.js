const Order = require("../models/order");
const Product = require("../models/product");

const ErrorHandler = require("../utils/errorHandler");
const catchAsyncErrors = require("../middlewares/catchAsyncErrors");

//Create a new order => /api/v1/order/new
exports.newOrder = catchAsyncErrors(async (req, res, next) => {
    const { 
        orderItems, 
        shippingInfo, 
        itemsPrice, 
        taxPrice, 
        shippingPrice,
        paymentInfo,
    } = req.body;

    let totalPrice = itemsPrice + taxPrice + shippingPrice;

    const order = await Order.create({
        orderItems, 
        shippingInfo, 
        itemsPrice, 
        taxPrice, 
        shippingPrice,
        paymentInfo,
        totalPrice,
        paidAt: Date.now(),
        user: req.user._id
    });

    return res.status(200).json({
        success: true,
        order
    })
})

// Get single order => /api/v1/order/:id
exports.getSingleOrder = catchAsyncErrors(async (req, res, next) => {
    const order = await (await Order.findById(req.params.id)).populate("user", "name email");

    if(!order) {
        return next(new ErrorHandler("No order found with this ID", 404));
    }

    return res.status(200).json({
        success: true,
        order
    })
})

// Get all orders => /api/v1/admin/orders/
exports.allOrders = catchAsyncErrors(async (req, res, next) => {
    const orders = await Order.find();

    let totalAmount = 0;

    orders.forEach(order => {
        totalAmount += order.totalPrice;
    })

    return res.status(200).json({
        success: true,
        totalAmount,
        orders
    })
})


// Get logged in user order => /api/v1/orders/me
exports.myOrders = catchAsyncErrors(async (req, res, next) => {
    const orders = await Order.find({ user: req.user.id });

    return res.status(200).json({
        success: true,
        orders
    })
})

// Update / Process order - ADMIN => /api/v1/admin/order/:id
exports.updateProcessOrder = catchAsyncErrors(async (req, res, next) => {
    const order = await Order.findById( req.params.id );

    if(order.orderStatus === "Delivered") {
        return next(new ErrorHandler("Bạn đã giao đơn hàng này", 400));
    }

    order.orderItems.forEach(async item => {
        await updateStock(item.product, item.quantity)
    })

    order.orderStatus = req.body.status;
    order.deliveredAt = Date.now();

    await order.save();

    return res.status(200).json({
        success: true,
        order
    })
})

async function updateStock(id, quantity) {
    const product = await Product.findById(id);

    product.stock = product.stock - quantity;

    await product.save({ validateBeforeSave: false });
}
