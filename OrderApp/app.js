const express = require("express");

const app = express();


const errorMiddleware = require("./middlewares/errors");
const cookieParser = require("cookie-parser");

app.use(express.json({ extend: false }));
app.use(cookieParser());



//Import all routes

const orders = require("./routes/order")

app.use(orders);

app.use(errorMiddleware)

module.exports = app;